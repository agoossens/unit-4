
public class math extends Courses {

	/**
	 * 
	 * @param newTitle -values from the Courses class which are used here
	 * @param newNumber
	 * @param newDescription
	 * @param newDepartment
	 */


	public math(String newTitle, int newNumber, String newDescription,
			String newDepartment) {
		super(newTitle, newNumber, newDescription, newDepartment);
		// TODO Auto-generated constructor stub
		/**
		 * assigns the values specifically to this class
		 */
	}
	
	public void setTitle (String titleMath)
	{
		super.setTitle(titleMath);;
	}
	public void setNumber (int numberMath)
	{
		super.setNumber(numberMath);;
	}
	public void setDescription (String descriptionMath)
	{
		super.setDescription(descriptionMath);
	}
	public void setDepartment (String departmentMath)
	{
		super.setDepartment(departmentMath);
	}

	public String toString()
	{
		return("title of the class:  " + super.getTitle() + "number of the class: " + super.getNumber() + "description of the class: " 
				+ super.getDescription() + "department: " + super.getDepartment());
		/**
		 * a toStrign method which prints out the specific values entered above
		 */
	}

}
