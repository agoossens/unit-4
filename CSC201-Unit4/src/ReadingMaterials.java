
public class ReadingMaterials {
	
	private int pageNumbers;
	private int numberOfAuthors;
	private String title;
	/**
	 * 
	 * @param newPageNumbers -the number of pages in the item 
	 * @param newNumberOfAuthors -number of authors
	 * @param newTitle - the title 
	 */
	
	public ReadingMaterials (int newPageNumbers, int newNumberOfAuthors, String newTitle)
	{
		pageNumbers = newPageNumbers;
		numberOfAuthors = newNumberOfAuthors;
		title = newTitle;
		/**
		 * constructor to get the values and assign the values
		 */
	}
	
	public int getPageNumbers() {
		return pageNumbers;
		/**
		 * getter method for page numbers
		 */
	}
	public void setPageNumbers(int pageNumbers) {
		this.pageNumbers = pageNumbers;
		/**
		 * setter method for page numbers
		 */
	}
	public int getNumberOfAuthors() {
		return numberOfAuthors;
	}
	public void setNumberOfAuthors(int numberOfAuthors) {
		this.numberOfAuthors = numberOfAuthors;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	

}
