import java.util.Scanner;
/**
 * Algoritm to solve the problem
 * 1)create private variables for the characteristics and create getters and setters
 * 2)create a constructor
 * 3) create different classes for each sport
 * 4) use super to assign the values to those classes
 * 5) create a toString method to call everything up
 * 6) create a demo method to chose which sport and to determine which class to use
 * @author alexgoossens
 *
 */

public class SportsStatistics {

private int score; 
private int ranking;
private int numberOfPlayersOnTeam;
private int gamesLeft;
/**
 * 
 * @param newScore -keeps track of the score of the game
 * @param newRanking - keeps track of the current ranking/leaderboard sport
 * @param newNumberOfPlayers - keeps track of how many players are on the team
 * @param newGamesLeft - keeps track of how many games are left in the seasons
 */

public SportsStatistics(int newScore, int newRanking, int newNumberOfPlayers, int newGamesLeft)
{
	score = newScore; 
	ranking = newRanking;
	numberOfPlayersOnTeam = newNumberOfPlayers;
	gamesLeft = newGamesLeft;
	/**
	 * creates a constructor to put the values in
	 */
	
}

public int getScore() {
	return score;
	/**
	 * getter method for the score
	 */
}
public void setScore(int score) {
	this.score = score;
	/**
	 * setter method for the score
	 */
}
public int getRanking() {
	return ranking;
	/**
	 * getter method for the ranking
	 */
}
public void setRanking(int ranking) {
	this.ranking = ranking;
	/**
	 * setter method for ther ranking
	 */
}
public int getNumberOfPlayers() {
	return numberOfPlayersOnTeam;
	/**
	 * getter method for the number of players
	 */
}
public void setNumberOfPlayers(int numberOfPlayers) {
	this.numberOfPlayersOnTeam = numberOfPlayers;
	/**
	 * setter method for the number of players
	 */
}
public int getGamesLeft() {
	return gamesLeft;
	/**
	 * getter method for the number of games left
	 */
}
public void setGamesLeft(int gamesLeft) {
	this.gamesLeft = gamesLeft;
	/**
	 * setter method for the number of games left
	 */
}


}


