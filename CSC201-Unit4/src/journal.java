
public class journal extends ReadingMaterials {
/**
 * 
 * @param newPageNumbers -brings these values from the reading materials class over
 * @param newNumberOfAuthors
 * @param newTitle
 */


	public journal(int newPageNumbers, int newNumberOfAuthors, String newTitle) {
		super(newPageNumbers, newNumberOfAuthors, newTitle);
	/**
	 * sets the values specifically to this class
	 */
	}
	public void setPageNumbers (int pageNumbersJournal)
	{
		super.setPageNumbers(pageNumbersJournal);
	}
	public void setNumberOfAuthors (int numberOfAuthorsJournal)
	{
		super.setNumberOfAuthors(numberOfAuthorsJournal);
	}
	public void setTitle(String titleJournal)
	{
		super.setTitle(titleJournal);
	}
	
	public String toString()
	{
		return("number of pages: " + super.getPageNumbers() + "number of authors: " + super.getNumberOfAuthors() + "name of title: " + super.getTitle());
	/**
	 * creates a toString method which prints out the values entered above
	 */
	}
}
