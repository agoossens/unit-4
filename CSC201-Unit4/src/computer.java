
public class computer extends ElectronicEquipment {
	/**
	 * 
	 * @param newWeight -uses the variables from the ElectronicEquipment class 
	 * @param newCost
	 * @param newPowerUsage
	 * @param newNameOfManufacturer
	 */
	public computer(int newWeight, int newCost, int newPowerUsage,
			String newNameOfManufacturer) {
		super(newWeight, newCost, newPowerUsage, newNameOfManufacturer);
		/**
		 * stores the variables specifically to this class
		 */
	}

	public void setWeight (int weightComputer)
	{
		super.setWeight(weightComputer);
	}
	public void setCost (int costComputer)
	{
		super.setCost(costComputer);
	}
	public void setPowerUsage (int powerUsageComputer)
	{
		super.setPowerUsage(powerUsageComputer);
	}
	public void setNameOfManufacturer (String nameOfManufacturerComputer)
	{
		super.setNameOfManufacturer(nameOfManufacturerComputer);
	}
	public String toString()
	{
		return ("weight: " + super.getWeight() + "cost: " + super.getCost() + "power usage: " 
				+ super.getPowerUsage() + "name of manufacturer" + super.getNameOfManufacturer());

		/**
		 * a toString method which prints out the variables entered above
		 */
	}
}
