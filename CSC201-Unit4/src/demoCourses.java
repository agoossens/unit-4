import java.util.Scanner;

public class demoCourses {
	/**
	 * runs the previous classes
	 */

	private static	Courses constructorToStoreValues;
	public static void main(String[] args) {

/**
 * switch statement which allows the user to chose which item
 * it will then go to that specific class and run it
 */

		System.out.println("Please choose a tech item.");
		System.out.println("1) math");
		System.out.println("2) english");
		System.out.println("3) history");
		Scanner keyboard = new Scanner(System.in);
		int choice = keyboard.nextInt();

		switch (choice){
		case 1:
		{
			System.out.println("Please enter the title of the math class.");
			String newTitle = keyboard.next();
			System.out.println("Please enter the number of the math class.");
			int newNumber = keyboard.nextInt();
			System.out.println("Please enter the description of the math class.");
			String newDescription = keyboard.next();
			System.out.println("Please enter the department of the math class.");
			String newDepartment = keyboard.next();


			constructorToStoreValues = new math(newTitle, newNumber, newDescription, newDepartment);
			System.out.println(constructorToStoreValues);
			break;
		}

		case 2: 
		{
			System.out.println("Please enter the title of the english class.");
			String newTitle = keyboard.next();
			System.out.println("Please enter the number of the english class.");
			int newNumber = keyboard.nextInt();
			System.out.println("Please enter the description of the class.");
			String newDescription = keyboard.next();
			System.out.println("Please enter the department of the class.");
			String newDepartment = keyboard.next();


			constructorToStoreValues = new english(newTitle, newNumber, newDescription, newDepartment);
			System.out.println(constructorToStoreValues);
			break;
		}
		case 3: 
		{
			System.out.println("Please enter the title of the history class.");
			String newTitle = keyboard.next();
			System.out.println("Please enter the number of the history class.");
			int newNumber = keyboard.nextInt();
			System.out.println("Please enter the description of the history class.");
			String newDescription = keyboard.next();
			System.out.println("Please enter the department of the history class.");
			String newDepartment = keyboard.next();


			constructorToStoreValues = new history(newTitle, newNumber, newDescription, newDepartment);
			System.out.println(constructorToStoreValues);
			break;
		}
		}


	}


}


