
public class history extends Courses {

	/**
	 * 
	 * @param newTitle -values from the Courses class which are used here
	 * @param newNumber
	 * @param newDescription
	 * @param newDepartment
	 */


	public history(String newTitle, int newNumber, String newDescription,
			String newDepartment) {
		super(newTitle, newNumber, newDescription, newDepartment);
		// TODO Auto-generated constructor stub
		/**
		 * assigns the values specifically to this class
		 */
	}
	
	public void setTitle (String titleHistory)
	{
		super.setTitle(titleHistory);;
	}
	public void setNumber (int numberHistory)
	{
		super.setNumber(numberHistory);;
	}
	public void setDescription (String descriptionHistory)
	{
		super.setDescription(descriptionHistory);
	}
	public void setDepartment (String departmentHistory)
	{
		super.setDepartment(departmentHistory);
	}

	public String toString()
	{
		return("title of the class:  " + super.getTitle() + "number of the class: " + super.getNumber() + "description of the class: " 
				+ super.getDescription() + "department: " + super.getDepartment());
		/**
		 * a toStrign method which prints out the specific values entered above
		 */
	}

}
