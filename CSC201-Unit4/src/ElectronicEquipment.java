
public class ElectronicEquipment {
	private int weight;
	private int cost;
	private int powerUsage;
	private String nameOfManufacturer;
	/**
	 * 
	 * @param newWeight -the weight of the object
	 * @param newCost - the cost of the object 
	 * @param newPowerUsage - the power usage in Watts of the object
	 * @param newNameOfManufacturer - the name of the manufacturer 
	 */
	public ElectronicEquipment (int newWeight, int newCost, int newPowerUsage, String newNameOfManufacturer)
	{
		weight = newWeight; 
		cost = newCost;
		powerUsage = newPowerUsage;
		nameOfManufacturer = newNameOfManufacturer;
		/**
		 * constructor to store the values
		 */
	}

	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public int getPowerUsage() {
		return powerUsage;
	}
	public void setPowerUsage(int powerUsage) {
		this.powerUsage = powerUsage;
	}
	public String getNameOfManufacturer() {
		return nameOfManufacturer;
	}
	public void setNameOfManufacturer(String nameOfManufacturer) {
		this.nameOfManufacturer = nameOfManufacturer;
	}




}
