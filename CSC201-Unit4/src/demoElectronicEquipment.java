import java.util.Scanner;

public class demoElectronicEquipment {
	/**
	 * runs the previous classes
	 */

	private static	ElectronicEquipment constructorToStoreValues;
	public static void main(String[] args) {

/**
 * switch statement which allows the user to chose which item
 * it will then go to that specific class and run it
 */

		System.out.println("Please choose a tech item.");
		System.out.println("1) computer");
		System.out.println("2) cellphone");
		System.out.println("3) pager");
		Scanner keyboard = new Scanner(System.in);
		int choice = keyboard.nextInt();

		switch (choice){
		case 1:
		{
			System.out.println("Please enter the weight of the computer.");
			int newWeight = keyboard.nextInt();
			System.out.println("Please enter the cost of the computer.");
			int newCost = keyboard.nextInt();
			System.out.println("Please enter the power usage in watts of the computer.");
			int newPowerUsage = keyboard.nextInt();
			System.out.println("Please enter the manufacturer name.");
			String newNameOfManufacturer = keyboard.next();


			constructorToStoreValues = new computer(newWeight, newCost, newPowerUsage, newNameOfManufacturer);
			System.out.println(constructorToStoreValues);
			break;
		}

		case 2: 
		{
			System.out.println("Please enter the weight of the cellPhone.");
			int newWeight = keyboard.nextInt();
			System.out.println("Please enter the cost of the cellPhone.");
			int newCost = keyboard.nextInt();
			System.out.println("Please enter the power usage in watts of the cellPhone.");
			int newPowerUsage = keyboard.nextInt();
			System.out.println("Please enter the manufacturer CellPhone.");
			String newNameOfManufacturer = keyboard.next();


			constructorToStoreValues = new cellPhone(newWeight, newCost, newPowerUsage, newNameOfManufacturer);
			System.out.println(constructorToStoreValues);
			break;
		}
		case 3: 
		{
			System.out.println("Please enter the weight of the pager.");
			int newWeight = keyboard.nextInt();
			System.out.println("Please enter the cost of the pager.");
			int newCost = keyboard.nextInt();
			System.out.println("Please enter the power usage in watts of the pager.");
			int newPowerUsage = keyboard.nextInt();
			System.out.println("Please enter the manufacturer name of the pager.");
			String newNameOfManufacturer = keyboard.next();


			constructorToStoreValues = new pager(newWeight, newCost, newPowerUsage, newNameOfManufacturer);
			System.out.println(constructorToStoreValues);
			break;
		}
		}


	}


}


