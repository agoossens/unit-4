
public class book extends ReadingMaterials {

	/**
	 * 
	 * @param newPageNumbers -brings these objects to this specific class
	 * @param newNumberOfAuthors
	 * @param newTitle
	 */
	public book(int newPageNumbers, int newNumberOfAuthors, String newTitle) {
		super(newPageNumbers, newNumberOfAuthors, newTitle);
		/**
		 * stores the values in this specific class
		 */
	}
	
	public void setPageNumbers (int pageNumbersBook)
	{
		super.setPageNumbers(pageNumbersBook);
	}
	public void setNumberOfAuthors (int numberOfAuthorsBook)
	{
		super.setNumberOfAuthors(numberOfAuthorsBook);
	}
	public void setTitle(String titleBook)
	{
		super.setTitle(titleBook);
	}
	public String toString()
	{
		return("number of pages: " + super.getPageNumbers() + "number of authors: " + super.getNumberOfAuthors() + "name of title: " + super.getTitle());
	/**
	 * a toStrign method which prints out the specific values entered above
	 */
	}

}
