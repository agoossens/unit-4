import java.util.Scanner;

public class demo {
/**
 * this is where the program will be run
 */
private static	SportsStatistics constructorToStoreValues;
/**
 * calls the constructor values and info from sportsStatistics class and uses it here
 * @param args
 */
	public static void main(String[] args) {

/**
 * switch statement with cases to determine the sport and what classes to call up
 */
		
			System.out.println("Please choose a sport.");
			System.out.println("1) soccer");
			System.out.println("2) football");
			System.out.println("3) hockey");
			Scanner keyboard = new Scanner(System.in);
			int choice = keyboard.nextInt();

			switch (choice){
			case 1:
				{
					System.out.println("Please enter the number of soccer goals.");
					int newScore = keyboard.nextInt();
					System.out.println("Please enter the current ranking of the soccer team.");
					int newRanking = keyboard.nextInt();
					System.out.println("Please enter the number of players on the team.");
					int newNumberOfPlayers = keyboard.nextInt();
					System.out.println("Please enter the number of games left.");
					int newGamesLeft = keyboard.nextInt();
					
					
					constructorToStoreValues = new soccer(newScore, newRanking, newNumberOfPlayers, newGamesLeft);
					System.out.println(constructorToStoreValues);
					break;
				}
				
			case 2: 
			{
				System.out.println("Please enter the number of touchdowns.");
				int newScore = keyboard.nextInt();
				System.out.println("Please enter the current ranking of the football team.");
				int newRanking = keyboard.nextInt();
				System.out.println("Please enter the number of players on the team.");
				int newNumberOfPlayers = keyboard.nextInt();
				System.out.println("Please enter the number of games left.");
				int newGamesLeft = keyboard.nextInt();
				System.out.println("Please enter the number of time outs left.");
				int newTimeOutsLeft = keyboard.nextInt();
				
				constructorToStoreValues = new football(newScore, newRanking, newNumberOfPlayers, newGamesLeft, newTimeOutsLeft);
				System.out.println(constructorToStoreValues);
				break;
			}
			case 3: 
			{
				System.out.println("Please enter the hockey points you have.");
				int newScore = keyboard.nextInt();
				System.out.println("Please enter the current ranking of the hockey team.");
				int newRanking = keyboard.nextInt();
				System.out.println("Please enter the number of players on the team.");
				int newNumberOfPlayers = keyboard.nextInt();
				System.out.println("Please enter the number of games left.");
				int newGamesLeft = keyboard.nextInt();
				System.out.println("Please enter the number of time outs left.");
				int newTimeOutsLeft = keyboard.nextInt();
				
				constructorToStoreValues = new hockey(newScore, newRanking, newNumberOfPlayers, newGamesLeft, newTimeOutsLeft);
				System.out.println(constructorToStoreValues);
				break;
			}
			}

			
		}
	

}
