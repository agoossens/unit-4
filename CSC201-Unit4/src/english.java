
public class english extends Courses {

	/**
	 * 
	 * @param newTitle -values from the Courses class which are used here
	 * @param newNumber
	 * @param newDescription
	 * @param newDepartment
	 */

	public english(String newTitle, int newNumber, String newDescription,
			String newDepartment) {
		super(newTitle, newNumber, newDescription, newDepartment);
		// TODO Auto-generated constructor stub
		/**
		 * assigns the values specifically to this class
		 */
	}


	public void setTitle (String titleEnglish)
	{
		super.setTitle(titleEnglish);;
	}
	public void setNumber (int numberEnglish)
	{
		super.setNumber(numberEnglish);;
	}
	public void setDescription (String descriptionEnglish)
	{
		super.setDescription(descriptionEnglish);
	}
	public void setDepartment (String departmentEnglish)
	{
		super.setDepartment(departmentEnglish);
	}

	public String toString()
	{
		return("title of the class:  " + super.getTitle() + "number of the class: " + super.getNumber() + "description of the class: " 
				+ super.getDescription() + "department: " + super.getDepartment());
		/**
		 * a toStrign method which prints out the specific values entered above
		 */
	}

}
