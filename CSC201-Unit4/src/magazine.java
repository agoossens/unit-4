
public class magazine extends ReadingMaterials {
	/**
	 * 
	 * @param newPageNumbers -brings these variables to this class
	 * @param newNumberOfAuthors
	 * @param newTitle
	 */

	public magazine(int newPageNumbers, int newNumberOfAuthors, String newTitle) {
		super(newPageNumbers, newNumberOfAuthors, newTitle);
		/**
		 * stores the values specifically to this class due to super
		 */
	
	}
	public void setPageNumbers (int pageNumbersMagazine)
	{
		super.setPageNumbers(pageNumbersMagazine);
	}
	public void setNumberOfAuthors (int numberOfAuthorsMagazine)
	{
		super.setNumberOfAuthors(numberOfAuthorsMagazine);
	}
	public void setTitle(String titleMagazine)
	{
		super.setTitle(titleMagazine);
	}
	public String toString()
	{
		return("number of pages: " + super.getPageNumbers() + "number of authors: " + super.getNumberOfAuthors() + "name of title: " + super.getTitle());
	/**
	 * prints out a string of the values entered above with other strings entered by the keyboad
	 */
	}
}
