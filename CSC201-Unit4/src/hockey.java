
public class hockey extends SportsStatistics {

	private int timeOutsLeft;
	/**
	 * this variable is only for this class, shows how many timeouts are left
	 * @return
	 */
	public int getTimeOutsLeft() {
		return timeOutsLeft;
		/**
		 * getter for the number of time outs
		 */
	}
	public void setTimeOutsLeft(int timeOutsLeft) {
		this.timeOutsLeft = timeOutsLeft;
		/**
		 * setter for the number of time outs
		 */
	}
	public hockey(int newScore, int newRanking, int newNumberOfPlayers,
			int newGamesLeft, int newTimeOutsLeft) {
		super(newScore, newRanking, newNumberOfPlayers, newGamesLeft);
		timeOutsLeft = newTimeOutsLeft;	
		/**
		 * constructor which uses super to store it in this class
		 */
	}
	public void setScore (int scoreHockey)
	{
		super.setScore(scoreHockey);
		/**
		 * sets the score value to this class
		 */
	}

	public void setRanking (int rankingHockey)
	{
		super.setRanking(rankingHockey);
		/**
		 * sets the ranking value to this class
		 */
	}
	public void setNumberOfPlayers (int numberOfPlayersHockey)
	{
		super.setNumberOfPlayers(numberOfPlayersHockey);
		/**
		 * sets the number of players to this class
		 */
	}
	public void setGamesLeft (int gamesLeftHockey)
	{
		super.setGamesLeft(gamesLeftHockey);
		/**
		 * sets the number of games left to this class
		 */
	}

	public String toString()
	{
		return ("score: " + super.getScore() + "ranking: " + super.getRanking() + "number of players on the team: " + super.getNumberOfPlayers()+"games left: " 
				+ super.getGamesLeft()) + "time outs left: " + timeOutsLeft;
	/**
	 * toString method which prints out the values entered above
	 */
	}

}
