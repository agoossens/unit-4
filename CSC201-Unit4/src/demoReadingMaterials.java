import java.util.Scanner;

public class demoReadingMaterials {

private static	ReadingMaterials constructorToStoreValues;
/**
 * uses the reading materials class
 * @param args
 */
	public static void main(String[] args) {

/**
 * a switch statement to chose which class to use 
 */
		
			System.out.println("Please choose a sport.");
			System.out.println("1) Book");
			System.out.println("2) Journal");
			System.out.println("3) Magazine");
			Scanner keyboard = new Scanner(System.in);
			int choice = keyboard.nextInt();

			switch (choice){
			case 1:
				{
					System.out.println("Please enter the number of pages in the book.");
					int newPageNumbers = keyboard.nextInt();
					System.out.println("Please enter the number of authors of the book.");
					int newNumberOfAuthors = keyboard.nextInt();
					System.out.println("Please enter the title of the book.");
					String newTitle = keyboard.next();

					
					constructorToStoreValues = new book(newPageNumbers, newNumberOfAuthors, newTitle);
					System.out.println(constructorToStoreValues);
					break;
				}
				
			case 2: 
			{
				System.out.println("Please enter the number of pages in the journal.");
				int newPageNumbers = keyboard.nextInt();
				System.out.println("Please enter the number of authors of the journal.");
				int newNumberOfAuthors = keyboard.nextInt();
				System.out.println("Please enter the title of the journal.");
				String newTitle = keyboard.next();

				
				constructorToStoreValues = new journal(newPageNumbers, newNumberOfAuthors, newTitle);
				System.out.println(constructorToStoreValues);
				break;
			}
			case 3: 
			{
				System.out.println("Please enter the number of pages in the magazine.");
				int newPageNumbers = keyboard.nextInt();
				System.out.println("Please enter the number of authors of the magazine.");
				int newNumberOfAuthors = keyboard.nextInt();
				System.out.println("Please enter the title of the magazine.");
				String newTitle = keyboard.next();

				
				constructorToStoreValues = new magazine(newPageNumbers, newNumberOfAuthors, newTitle);
				System.out.println(constructorToStoreValues);
				break;
			}
			}

			
		}
	

}
