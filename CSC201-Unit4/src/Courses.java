
public class Courses {
	
	private String title;
	private int number;
	private String description;
	private String department;
	/**
	 * 
	 * @param newTitle - gets the title of the course 
	 * @param newNumber - gets the course number
	 * @param newDescription - gets the description of the course
	 * @param newDepartment - gets the department of the course
	 */
	
	public Courses (String newTitle, int newNumber, String newDescription, String newDepartment)
	{
		title = newTitle;
		number = newNumber;
		description = newDescription;
		department = newDepartment;
		/**
		 * constructor to get the values and assign the values
		 */
	}
	

	public int getNumber() {
		return number;
	}


	public void setNumber(int number) {
		this.number = number;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDepartment() {
		return department;
	}
	
	public void setDepartment(String department) {
		this.department = department;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	

}
