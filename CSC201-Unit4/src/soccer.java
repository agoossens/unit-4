
public class soccer extends SportsStatistics {

	public soccer(int newScore, int newRanking, int newNumberOfPlayers,
			int newGamesLeft) {
		super(newScore, newRanking, newNumberOfPlayers, newGamesLeft);
	/**
	 * uses super to assign the constructor values to this specific class 
	 */
	}

	public void setScore (int scoreSoccer)
	{
		super.setScore(scoreSoccer);
		/**
		 * assigns the value to score
		 */
	}
	public void setRanking (int rankingSoccer)
	{
		super.setRanking(rankingSoccer);
		/**
		 * assigns the value to ranking
		 */
	}
	public void setNumberOfPlayers (int numberOfPlayersSoccer)
	{
		super.setNumberOfPlayers(numberOfPlayersSoccer);
		/**
		 * assigns the value to the number of players
		 */
	}
	public void setGamesLeft (int gamesLeftSoccer)
	{
		super.setGamesLeft(gamesLeftSoccer);
		/**
		 * assigns the value to the number of games left
		 */
	}
	
	public String toString()
	{
		return ("score: " + super.getScore() + "ranking: " + super.getRanking() + "number of players on the team: " + super.getNumberOfPlayers()+"games left: " + super.getGamesLeft());
	/**
	 * toString method which puts out the string with the values entered previously in the class
	 */
	}

}
