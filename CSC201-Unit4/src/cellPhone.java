
public class cellPhone extends ElectronicEquipment {
	/**
	 * 
	 * @param newWeight - uses the variables from the ElectronicEquipment class 
	 * @param newCost
	 * @param newPowerUsage
	 * @param newNameOfManufacturer
	 */
	public cellPhone(int newWeight, int newCost, int newPowerUsage,
			String newNameOfManufacturer) {
		super(newWeight, newCost, newPowerUsage, newNameOfManufacturer);
		// TODO Auto-generated constructor stub
		/**
		 * stores the variables specifically to this class
		 */
	}

	public void setWeight (int weightCellPhone)
	{
		super.setWeight(weightCellPhone);
	}
	public void setCost (int costCellPhone)
	{
		super.setCost(costCellPhone);
	}
	public void setPowerUsage (int powerUsageCellPhone)
	{
		super.setPowerUsage(powerUsageCellPhone);
	}
	public void setNameOfManufacturer (String nameOfManufacturerCellPhone)
	{
		super.setNameOfManufacturer(nameOfManufacturerCellPhone);
	}
	public String toString()
	{
		return ("weight: " + super.getWeight() + "cost: " + super.getCost() + "power usage: " 
				+ super.getPowerUsage() + "name of manufacturer" + super.getNameOfManufacturer());
		/**
		 * a toString method which prints out the variables entered above
		 */
	}
}
