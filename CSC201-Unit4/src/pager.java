
public class pager extends ElectronicEquipment {
	/**
	 * 
	 * @param newWeight -uses the variables from the ElectronicEquipment class 
	 * @param newCost
	 * @param newPowerUsage
	 * @param newNameOfManufacturer
	 */

	public pager(int newWeight, int newCost, int newPowerUsage,
			String newNameOfManufacturer) {
		super(newWeight, newCost, newPowerUsage, newNameOfManufacturer);
		// TODO Auto-generated constructor stub
		/**
		 * stores the variables specifically to this class
		 */
	}

	public void setWeight (int weightPager)
	{
		super.setWeight(weightPager);
	}
	public void setCost (int costPager)
	{
		super.setCost(costPager);
	}
	public void setPowerUsage (int powerUsagePager)
	{
		super.setPowerUsage(powerUsagePager);
	}
	public void setNameOfManufacturer (String nameOfManufacturerPager)
	{
		super.setNameOfManufacturer(nameOfManufacturerPager);
	}
	public String toString()
	{
		return ("weight: " + super.getWeight() + "cost: " + super.getCost() + "power usage: " 
				+ super.getPowerUsage() + "name of manufacturer" + super.getNameOfManufacturer());
		/**
		 * a toString method which prints out the variables entered above
		 */
	}
}
