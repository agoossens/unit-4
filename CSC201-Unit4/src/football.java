
public class football extends SportsStatistics {

	private int timeOutsLeft;
	/**
	 * 
	 * @param newScore 
	 * @param newRanking
	 * @param newNumberOfPlayers
	 * @param newGamesLeft
	 * @param newTimeOutsLeft -only assigned for this class
	 */

	public football(int newScore, int newRanking, int newNumberOfPlayers,
			int newGamesLeft, int newTimeOutsLeft) {
		super(newScore, newRanking, newNumberOfPlayers, newGamesLeft);
		timeOutsLeft = newTimeOutsLeft;	
		/**
		 * sets the constructor values to this class
		 */
	}
	
	
	public int getTimeOutsLeft() {
		return timeOutsLeft;
		/**
		 * getter for timeouts left for this class
		 */
	}


	public void setTimeOutsLeft(int timeOutsLeft) {
		this.timeOutsLeft = timeOutsLeft;
		/**
		 * setter method for this class
		 */
	}


	public void setScore (int scoreFootball)
	{
		super.setScore(scoreFootball);
		/**
		 * setter method for the score of this class
		 */
	}

	public void setRanking (int rankingFootball)
	{
		super.setRanking(rankingFootball);
		/**
		 * setter method for the ranking of this class
		 */
	}
	public void setNumberOfPlayers (int numberOfPlayersFootball)
	{
		super.setNumberOfPlayers(numberOfPlayersFootball);
		/**
		 * setter method for the number of players for this class
		 */
	}
	public void setGamesLeft (int gamesLeftFootball)
	{
		super.setGamesLeft(gamesLeftFootball);
		/**
		 * setter method for the games left of this clas
		 */
	}

	public String toString()
	{
		return ("score: " + super.getScore() + "ranking: " + super.getRanking() + "number of players on the team: " + super.getNumberOfPlayers()+"games left: " 
	+ super.getGamesLeft()) + "time outs left: " + timeOutsLeft;
		/**
		 * toString method which prints out the values entered above
		 */
	}
}
